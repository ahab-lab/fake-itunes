FROM openjdk:15
ADD target/fake_itunes-0.0.1-SNAPSHOT.jar tl-dk-sqlite.jar
ENTRYPOINT ["java", "-jar", "tl-dk-sqlite.jar"]