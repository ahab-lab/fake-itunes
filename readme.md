# Fake itunes
Fake itunes is a web application that uses Thymeleaf for front-end and Sping in the back-end. The application provides a simple search engine that allows users to search for a track by its name in [Chinhook database](https://github.com/lerocha/chinook-database). It also has an API that handles requests related to customers.



## User view
The user has two pages to navigate through:

View 1: Search
This page is what the users receive on the launch of the web application. It provides a search bar and 3 lists of 5 random albums, artists and tracks. The page looks as below:
![](preview/page1.png)

View 2: Search result
This page will show the search result if they exist in the database. The result will be as in the figure below:
![](preview/page2.png)


## API 
#####  Endpoint definitions 

```link
/api/customers
```
- Request type: Get 
- Purpose: To fetch all customers in the database, this shows the customer id, first name, last name, country, postal code, phone number and email.

Response example:
```json
{
        "id": 1,
        "firstName": "Luís",
        "lastName": "Gonçalves",
        "country": "Brazil",
        "postalCode": "12227-000",
        "phone": "+55 (12) 3923-5555",
        "email": "luisg@embraer.com.br"
}
```
---
```link
/api/customers/total/countries
```
- Request type: Get 
- Purpose: This gives a list that contains number of customers in each country.

- Response example:

```json
 {
        "country": "USA",
        "totalCustomers": 13
 }
```
---
```link
/api/customers/highest-spenders
```
- Request type: Get
- Purpose: Return a list of the highest spenders customers based on the the number of tracks they have purchased.
- Response example: 
```json
{
        "id": 6,
        "firstName": "Helena",
        "lastName": "Holý",
        "country": "Czech Republic",
        "postalCode": "14300",
        "phone": "+420 2 4177 0449",
        "email": "hholy@gmail.com"
}
```
---
```
/api/customers
```
- Request type: Put
- Purpose: To update a customer by his id
---

```link
/api/customers/56/popular/genre
```
- Request type: Get
- Purpose: returns the most popular genre in the system by looking at the number of tracks that are associated with a given genre. In case of a tie, returns a list of both genres.

```link
/api/customers
```
Request type: Put
Purpose: To update a customer by his id
---
```link
/api/customers/56/popular/genre
```
- Request type: Get
- Purpose: returns the most popular genre in the system by looking at the number of tracks that are associated with a given genre. In case of a tie, returns a list of both genres.
- Response example: 
```json
{
        "name": "Alternative & Punk",
        "total": 9
},
{
        "name": "Rock",
        "total": 9
}
```
## Heroku
The application has been uploaded to Heroku, and can be accessed by the link below:
```
https://experis-denmark-fakeitunes.herokuapp.com/
```
Link to [Docker container deployed on Heroku](https://tldksqlite.herokuapp.com/)
