package com.experis.fake_itunes.data_access;

import com.experis.fake_itunes.models.Customer;
import com.experis.fake_itunes.models.PopularGenre;
import com.experis.fake_itunes.models.TotalCustomersInCountries;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * this class contains the needed logic to fetch and insert data to database that are related to the customer api
 */
public class CustomerRepository {

    private final ConnectionHelper connectionHelper;

    public CustomerRepository() {
        connectionHelper = new ConnectionHelper();
    }

    /**
     * return a list of all available customers
     */
    public ArrayList<Customer> getCustomers() {
        ArrayList<Customer> customers = new ArrayList<Customer>();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT CustomerId,FirstName,LastName,Country," +
                            "PostalCode,Phone,Email FROM Customer");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    /**
     * update the given customer by his id
     * @param customer new customer data
     * @return true if updated successfully
     */
    public boolean updateCustomer(Customer customer) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE Customer SET FirsTName =?, LastName =?, Country =?,  PostalCode =?, Phone =?, Email =? WHERE CustomerId =?;");

            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getId());

            preparedStatement.executeUpdate();

            System.out.println("Customer with id: " + customer.getId() + " has been updated successfully.");

            return true;//TODO does it close connection when true?
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return false;
    }

    /**
     * add a new customer to the database
     * @param customer the customer to be added
     * @return true if added successfully otherwise false
     */
    public boolean addNewCustomer(Customer customer) {
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO Customer" +
                            " (FirstName, LastName, Country, PostalCode, Phone, Email) " +
                            " VALUES (?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLastName());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostalCode());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            // Execute Statement
            preparedStatement.executeUpdate();

            System.out.println("A new customer has been added successfully.");

            return true;
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return false;
    }

    /**
     * return a list of countries along side the number of customers in each country
     */
    public ArrayList<TotalCustomersInCountries> getNumbOfCustomersInCountries() {

        Connection conn = null;
        ArrayList<TotalCustomersInCountries> records = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Country, COUNT(Country) as Total FROM Customer" +
                            " GROUP BY Country ORDER BY Country DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                records.add(new TotalCustomersInCountries(
                        resultSet.getString("Country"),
                        resultSet.getInt("Total")
                ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return records;
    }

    /**
     * return a list of highest spenders in database by looking at the number of track a customer has purchased
     */
    public ArrayList<Customer> getHighestSpendersList() {
        ArrayList<Customer> customers = new ArrayList<>();
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, Customer.Country, Customer.PostalCode, Customer.Phone, Customer.Email, SUM(invoice.Total) as `sum` " +
                            " FROM Customer ,invoice  WHERE Customer.CustomerId = Invoice.CustomerId group by Customer.CustomerId ORDER BY `sum` DESC");
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getInt("CustomerId"),
                                resultSet.getString("FirstName"),
                                resultSet.getString("LastName"),
                                resultSet.getString("Country"),
                                resultSet.getString("PostalCode"),
                                resultSet.getString("Phone"),
                                resultSet.getString("Email")
                        ));
            }
        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return customers;
    }

    /**
     * return a list of most popular genre by looking at the
     * @param customerId
     * @return
     */
    public ArrayList<PopularGenre> getMostPopularGenre(int customerId) {
        Connection conn = null;
        ArrayList<PopularGenre> genreList = new ArrayList<>();

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Genre.Name AS Genre, COUNT(Genre.Name)  AS Total " +
                            "FROM Genre " +
                            "JOIN Track, InvoiceLine, Invoice " +
                            "WHERE Genre.GenreId = Track.GenreId " +
                            "AND Track.TrackId = InvoiceLine.TrackId " +
                            "AND InvoiceLine.InvoiceId = Invoice.InvoiceId " +
                            "AND Invoice.CustomerId = ? " +
                            "GROUP BY Genre.Name " +
                            "ORDER BY total  DESC");
            preparedStatement.setInt(1, customerId);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            int max = 0;

            // Process Results
            while (resultSet.next()) {
                if (max <= resultSet.getInt("Total")) {
                    genreList.add(new PopularGenre(
                            resultSet.getString("Genre"),
                            resultSet.getInt("Total")
                    ));
                    max = resultSet.getInt("Total");
                }
            }

        } catch (Exception ex) {
            System.out.println("Something went wrong...");
            System.out.println(ex.toString());
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                System.out.println("Something went wrong while closing connection.");
                System.out.println(ex.toString());
            }
        }
        return genreList;
    }
}