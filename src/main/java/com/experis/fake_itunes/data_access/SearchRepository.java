package com.experis.fake_itunes.data_access;

import com.experis.fake_itunes.logging.Logger;
import com.experis.fake_itunes.models.Track;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * this class handles the database logic to fetch data from database for the thymeleaf pages
 */
public class SearchRepository {

    private final ConnectionHelper connectionHelper;

    public SearchRepository() {
        connectionHelper = new ConnectionHelper();
    }

    /**
     * @return a list of 5 random artists in database
     */
    public ArrayList<String> getRandomArtists() {
        ArrayList<String> artists = new ArrayList<>();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            Logger.logToConsole("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT `Name` FROM Artist ORDER BY RANDOM() LIMIT ?;");
            preparedStatement.setInt(1, 5);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                artists.add(resultSet.getString("Name"));
            }
            Logger.logToConsole("a list of 5 random artists has been fetched from database.");
        } catch (Exception ex) {
            Logger.logToConsole("Something went wrong while fetching 5 random artists from database...");
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                Logger.logToConsole("Something went wrong while closing connection...");
            }
        }
        return artists;
    }

    /**
      * @return a list of 5 random genres in database
     */
    public ArrayList<String> getRandomGenre() {
        ArrayList<String> genres = new ArrayList<>();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            Logger.logToConsole("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT `Name` FROM Genre ORDER BY RANDOM() LIMIT ?;");
            preparedStatement.setInt(1, 5);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                genres.add(resultSet.getString("Name"));
            }
        } catch (Exception ex) {
            Logger.logToConsole("Something went wrong while fetching 5 random genres from database...");
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                Logger.logToConsole("Something went wrong while closing connection.");
            }
        }

        return genres;
    }

    /**
     * @return a list of 5 random tracks in database
     */
    public ArrayList<Track> getRandomTracks() {
        ArrayList<Track> tracks = new ArrayList<>();
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            System.out.println("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement = //TODO the query could be simplified
                    conn.prepareStatement("SELECT Track.Name AS Track, Artist.Name AS Artist, Album.Title As Album, Genre.Name AS Genre, Track.Milliseconds AS Length, Track.bytes AS `Size`, Track.UnitPrice AS price " +
                            "FROM Track " +
                            "JOIN Genre, Album, Artist " +
                            "WHERE Track.AlbumId = Album.AlbumId " +
                            "AND Album.ArtistId = Artist.ArtistId " +
                            "AND Track.GenreId = Genre.GenreId " +
                            "ORDER BY RANDOM() " +
                            "LIMIT ?;");

            preparedStatement.setInt(1, 5);
            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();
            // Process Results
            while (resultSet.next()) {
                tracks.add(new Track(resultSet.getString("Track"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre"),
                        resultSet.getInt("Length"),
                        resultSet.getInt("Size"),
                        resultSet.getDouble("Price")));
            }
        } catch (Exception ex) {
            Logger.logToConsole("Something went wrong while fetching 5 random tracks from database...");
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                Logger.logToConsole("Something went wrong while closing connection.");
            }
        }

        return tracks;
    }

    /**
     * @param trackName
     * @return a list of matching tracks with the given track name
     */
    public ArrayList<Track> getMatchingTrack(String trackName) {
        ArrayList<Track> tracks = new ArrayList<>();
        trackName = "%" + trackName + "%";
        Connection conn = null;

        try {
            conn = DriverManager.getConnection(connectionHelper.URL);
            Logger.logToConsole("Connection to SQLite has been established.");

            // Prepare Statement
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT Track.Name AS Track, Artist.Name AS Artist, Album.Title As Album, Genre.Name AS Genre, Track.Milliseconds AS length, Track.bytes AS size, Track.UnitPrice AS price " +
                            "FROM Track " +
                            "JOIN Album, Artist, Genre " +
                            "WHERE Track.AlbumId = Album.AlbumId " +
                            "AND Album.ArtistId = Artist.ArtistId " +
                            "AND Track.GenreId = Genre.GenreId " +
                            "AND Track.Name LIKE  ?;");
            preparedStatement.setString(1, trackName);

            // Execute Statement
            ResultSet resultSet = preparedStatement.executeQuery();

            // Process Results
            while (resultSet.next()) {
                tracks.add(new Track(resultSet.getString("Track"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre"),
                        resultSet.getInt("Length"),
                        resultSet.getInt("Size"),
                        resultSet.getDouble("Price"))
                );
            }
        } catch (Exception ex) {
            Logger.logToConsole("Something went wrong while fetching a list of matching tracks...");
        } finally {

            try {
                // Close Connection
                if (conn != null)
                    conn.close();
            } catch (Exception ex) {
                Logger.logToConsole("Something went wrong while closing connection.");
            }
        }

        return tracks;
    }
}