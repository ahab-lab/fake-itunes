package com.experis.fake_itunes.models;

public class Track {

    private String name;
    private String artist;
    private String album;
    private String genre;
    private int length;  // in milliseconds
    private int size;    //in bytes
    private double price;

    public Track(String name, String artist, String album, String genre, int length, int size, double price) {
        this.name = name;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
        this.length = length;
        this.size = size;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getAlbum() {
        return album;
    }

    public String getGenre() {
        return genre;
    }

    public int getLength() {
        return length;
    }

    public int getSize() {
        return size;
    }

    public double getPrice() {
        return price;
    }
}