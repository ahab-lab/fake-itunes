package com.experis.fake_itunes.models;

public class TotalCustomersInCountries {
    String country;
    int totalCustomers;

    public TotalCustomersInCountries(String country, int totalCustomers) {
        this.country = country;
        this.totalCustomers = totalCustomers;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public int getTotalCustomers() {
        return totalCustomers;
    }

    public void setTotalCustomers(int totalCustomers) {
        this.totalCustomers = totalCustomers;
    }
}
