package com.experis.fake_itunes.logging;

/**
 * this class output the received message to the console
 */
public class Logger {

    public static void logToConsole(String message) {
        System.out.println(message);
    }
}