package com.experis.fake_itunes.controllers;

import com.experis.fake_itunes.data_access.CustomerRepository;
import com.experis.fake_itunes.models.Customer;
import com.experis.fake_itunes.models.PopularGenre;
import com.experis.fake_itunes.models.TotalCustomersInCountries;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

/**
 * this class controls all incoming HTTP requests that are related to customers
 */
@RestController
@RequestMapping("/api/")
public class CustomerController {
    CustomerRepository cRepo = new CustomerRepository();

    @GetMapping("/customers")
    public ArrayList<Customer> getCustomers() {
        return cRepo.getCustomers();
    }

    @PostMapping("/customers")
    public boolean addNewCustomer(@RequestBody Customer customer) {

        return cRepo.addNewCustomer(customer);
    }

    @PutMapping("/customers")
    public boolean updateCustomer(@RequestBody Customer customer) {

        return cRepo.updateCustomer(customer);
    }

    @GetMapping("/customers/total/countries")
    public ArrayList<TotalCustomersInCountries> getNumbOfCustomersInCountries() {

        return cRepo.getNumbOfCustomersInCountries();
    }

    @GetMapping("/customers/highest-spenders")
    public ArrayList<Customer> getHighestSpenders() {

        return cRepo.getHighestSpendersList();
    }

    @GetMapping("/customers/{customerId}/popular/genre")
    public ArrayList<PopularGenre> getMostPopularGenre(@PathVariable("customerId") int customerId) {

        return cRepo.getMostPopularGenre(customerId);
    }
}