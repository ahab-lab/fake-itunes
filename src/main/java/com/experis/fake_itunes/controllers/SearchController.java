package com.experis.fake_itunes.controllers;

import com.experis.fake_itunes.data_access.SearchRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * This class handles the requests from thymeleaf pages
 */
@Controller
public class SearchController {

    SearchRepository searchRepository = new SearchRepository();

    @GetMapping(value = "/")
    public String search(Model model) {
        model.addAttribute("artists", searchRepository.getRandomArtists());
        model.addAttribute("genres", searchRepository.getRandomGenre());
        model.addAttribute("tracks", searchRepository.getRandomTracks());
        return "index";
    }

    @RequestMapping("/view-search-result")
    public String search(Model model, @RequestParam String term) {
        System.out.println(term);
        model.addAttribute("tracks", searchRepository.getMatchingTrack(term));

        return "view-search-result";
    }
}